import torch.nn as nn

from nets.senet import *
from nets.pooling import MAC, SPoC, GeM, RMAC, GeMmp, Rpool


POOLING = {
    'mac'   : MAC,
    'spoc'  : SPoC,
    'gem'   : GeM,
    'gemmp' : GeMmp,
    'rmac'  : RMAC,
}


def se_resnext50_32x4d(regional, whitening, input_size, embed_dim, gds):
    pooling_layers = {descriptor: POOLING[descriptor]() for descriptor in gds}

    layers = dict()
    for descriptor in pooling_layers:
        if regional:
            rpool = pooling_layers[descriptor]
            rwhiten = nn.Linear(2048, 2048, bias=True)

            pool = Rpool(rpool, rwhiten)
            layers[descriptor] = pool

    if whitening:
        whiten = nn.Linear(2048 * len(gds), embed_dim, bias=True)
    else:
        whiten = None

    layers = nn.ModuleDict(layers)

    model = SENet(SEResNeXtBottleneck, [3, 4, 6, 3], groups=32, reduction=16,
                  inplanes=64, input_3x3=False, embed_dim=embed_dim,
                  downsample_kernel_size=1, downsample_padding=0,
                  input_size=input_size, pool=layers, whiten=whiten)
    return model
