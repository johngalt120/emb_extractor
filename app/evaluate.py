import torch
import numpy as np
import auxiliaries as aux
import random


def find_closest_images(model, dataloader, categories_num, args):
    all_cats = list(dataloader.dataset.image_dict.keys())
    categories = random.choices(all_cats, k=categories_num)

    target_images = list()

    for cat in categories:
        sample = random.choice(dataloader.dataset.image_dict[cat])
        if sample is not None:
            target_images.append(sample)

    image_paths = np.array(dataloader.dataset.image_list)

    with torch.no_grad():
        feature_matrix_all = aux.eval_metrics_one_dataset(model, dataloader, device=args.device, args=args)

        aux.recover_closest_one_dataset(feature_matrix_all, image_paths, save_path='/app/pairs3.png',
                                        n_closest=args.n_closest, n_image_samples=len(target_images),
                                        target_images=target_images)