from nets.se_resnet import se_resnext50_32x4d


def networkselect(args):
    if args.arch == 'se_resnext50':
        network = se_resnext50_32x4d(regional=args.regional, gds=args.gds, whitening=args.whitening,
                                     input_size=args.input_size, embed_dim=args.embed_dim)
    else:
        raise Exception('Network {} not available!'.format(args.arch))
    return network