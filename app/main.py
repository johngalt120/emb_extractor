import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import os, sys, argparse
import torch
import datasets as data
import netlib
import evaluate as eval

from torchvision import transforms
from PIL import Image
from torch.nn.parallel import DataParallel as DP


def parse(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', default='Amazon', type=str, help='Dataset to use.')
    parser.add_argument('--input_size', default=280, type=int, help='Image size.')
    parser.add_argument('--device', default='cuda', type=str)
    parser.add_argument('--embed_dim', default=2048, type=int,
                        help='Embedding dimensionality of the network')
    parser.add_argument('--source_path', default=os.getcwd() + '/Datasets', type=str, help='Path to training data.')
    parser.add_argument('--arch', default='se_resnext50', type=str)
    parser.add_argument('--bs', default=24, type=int, help='batch size')
    parser.add_argument('--checkpoint_path', default='pretrained/checkpoint.pth.tar', type=str,
                        help='Path to pretrained model')
    parser.add_argument('--kernels',           default=8,        type=int,   help='Number of workers for pytorch dataloader.')
    parser.add_argument('--categories_num', default=10, type=int, help='num categories for inference mode')
    parser.add_argument('--imgs_per_cat', default=2, type=int, help='num images per category for inference mode')
    parser.add_argument('--n_closest', default=10, type=int, help='number of closest images to target')
    parser.add_argument('--gds', metavar='POOL', nargs='+', default='gem',
                        choices=['mac', 'spoc', 'gem', 'gemmp'])
    parser.add_argument('--img_paths', type=str, help='path to images for getting embedding')
    parser.add_argument('--regional', default=True, help='train model with regional pooling using fixed grid')
    parser.add_argument('--whitening', default=True, help='train model with learnable whitening (linear layer) after the pooling')

    return parser.parse_args()


def get_embeddings(model, device, img_paths, input_size):
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    transf_list = list()

    transf_list.extend([transforms.Resize(int(input_size * 0.9))])

    transf_list.extend([transforms.ToTensor(), normalize])
    transform = transforms.Compose(transf_list)

    embeddings = list()

    for root, _, files in os.walk(img_paths):
        for file in files:
            full_path = os.path.join(root, file)

            img = Image.open(full_path)

            if len(img.size) == 2:
                img = img.convert('RGB')

            img_tensor = transform(img)

            emb = model(img_tensor.to(device)[None, ...])
            emb = emb.cpu().detach().numpy()
            embeddings.append(emb)

    return embeddings


def main():
    args = parse(sys.argv[1:])

    args.source_path += '/' + args.dataset

    model = netlib.networkselect(args)
    if model: model.share_memory()

    gpus_num = torch.cuda.device_count()

    if gpus_num > 1:
        model = DP(model).cuda()
        args.bs = args.bs * gpus_num
    else:
        model = model.cuda()

    dataloader = data.give_dataloaders(args.dataset, args)
    args.num_classes = len(dataloader['evaluation'].dataset.avail_classes)

    model.load_state_dict(torch.load(args.checkpoint_path)['state_dict'])
    model.eval()

    embeddings = get_embeddings(model=model, device=args.device, img_paths=args.img_paths, input_size=args.input_size)

    print(embeddings)

    '''eval.find_closest_images(model=model, dataloader=dataloader['evaluation'], categories_num=args.categories_num,
                             args=args)'''


if __name__ == "__main__":
    main()
