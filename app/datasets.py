import copy
import torch
import os
import numpy as np

from torch.utils.data import Dataset
from PIL import Image
from torchvision import transforms


def give_dataloaders(dataset, args):
    """
    Args:
        dataset: string, name of dataset for which the dataloaders should be returned.
        args:     argparse.Namespace, contains all training-specific parameters.
    Returns:
        dataloaders: dict of dataloaders for training, testing and evaluation on training.
    """
    if args.dataset == 'Amazon':
        datasets = give_AmazonProducts_dataset(args)
    else:
        raise Exception('No Dataset >{}< available!'.format(dataset))

    dataloaders = {}
    for key, dataset in datasets.items():
        is_val = dataset.is_validation
        dataloaders[key] = torch.utils.data.DataLoader(dataset, batch_size=args.bs, num_workers=args.kernels,
                                                       shuffle=not is_val, pin_memory=True, drop_last=not is_val)

    return dataloaders


def give_AmazonProducts_dataset(opt, threshold=4):
    img_sourcepath = opt.source_path+'/images'

    image_classes = sorted([category for category in os.listdir(img_sourcepath) if
                            len(os.listdir(os.path.join(img_sourcepath, category))) >= threshold])
    conversion = {enum: category for enum, category in enumerate(os.listdir(img_sourcepath))}

    image_list = {enum: sorted([os.path.join(img_sourcepath, category, fname) for fname in os.listdir(
        os.path.join(img_sourcepath, category))]) for enum, category in enumerate(image_classes)}

    image_list = [[(key, img_path) for img_path in image_list[key]] for key in image_list.keys()]
    image_list = [x for y in image_list for x in y]

    image_dict = {}
    for key, img_path in image_list:
        if not key in image_dict.keys():
            image_dict[key] = []
        image_dict[key].append(img_path)

    print(f"number of categories: {len(image_dict)}")

    eval_dataset = BaseTripletDataset(image_dict, opt, is_validation=True)
    eval_dataset.conversion = conversion

    return {'evaluation': eval_dataset}


class BaseTripletDataset(Dataset):
    """
    Dataset class to provide (augmented) correctly prepared training samples corresponding to standard DML literature.
    This includes normalizing to ImageNet-standards, and Random & Resized cropping of shapes 224 for ResNet50 and 227 for
    GoogLeNet during Training. During validation, only resizing to 256 or center cropping to 224/227 is performed.
    """
    def __init__(self, image_dict, opt, samples_per_class=8, is_validation=False):
        """
        Dataset Init-Function.

        Args:
            image_dict:         dict, Dictionary of shape {class_idx:[list of paths to images belong to this class] ...} providing all the training paths and classes.
            opt:                argparse.Namespace, contains all training-specific parameters.
            samples_per_class:  Number of samples to draw from one class before moving to the next when filling the batch.
            is_validation:      If is true, dataset properties for validation/testing are used instead of ones for training.
        Returns:
            Nothing!
        """
        self.input_size = opt.input_size
        self.n_files = np.sum([len(image_dict[key]) for key in image_dict.keys()])

        self.is_validation = is_validation

        self.pars = opt
        self.image_dict = image_dict

        self.avail_classes = sorted(list(self.image_dict.keys()))

        self.image_dict = {i: self.image_dict[key] for i, key in enumerate(self.avail_classes)}
        self.avail_classes = sorted(list(self.image_dict.keys()))

        if not self.is_validation:
            self.samples_per_class = samples_per_class
            self.current_class = np.random.randint(len(self.avail_classes))
            self.classes_visited = [self.current_class, self.current_class]
            self.n_samples_drawn = 0

        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        transf_list = []
        if not self.is_validation:
            transf_list.extend([transforms.Resize(self.input_size),
                                transforms.RandomResizedCrop(size=int(self.input_size*0.9)),
                                transforms.ColorJitter(brightness=0.5, contrast=0.5, saturation=0, hue=0),
                                transforms.RandomHorizontalFlip(0.5)])
        else:
            transf_list.extend([transforms.Resize(self.input_size),
                                transforms.CenterCrop(int(self.input_size*0.9))])

        transf_list.extend([transforms.ToTensor(), normalize])
        self.transform = transforms.Compose(transf_list)

        self.image_list = [[(x,key) for x in self.image_dict[key]] for key in self.image_dict.keys()]
        self.image_list = [x for y in self.image_list for x in y]

        self.is_init = True

    def ensure_3dim(self, img):
        """
        Function that ensures that the input img is three-dimensional.

        Args:
            img: PIL.Image, image which is to be checked for three-dimensionality (i.e. if some images are black-and-white in an otherwise coloured dataset).
        Returns:
            Checked PIL.Image img.
        """
        if len(img.size) == 2:
            img = img.convert('RGB')
        return img

    def __getitem__(self, idx):
        """
        Args:
            idx: Sample idx for training sample
        Returns:
            tuple of form (sample_class, torch.Tensor() of input image)
        """
        if self.is_init:
            self.current_class = self.avail_classes[idx%len(self.avail_classes)]
            self.is_init = False

        if not self.is_validation:
            if self.samples_per_class == 1:
                return self.image_list[idx][-1], self.transform(self.ensure_3dim(Image.open(self.image_list[idx][0])))

            if self.n_samples_drawn == self.samples_per_class:
                counter = copy.deepcopy(self.avail_classes)
                for prev_class in self.classes_visited:
                    if prev_class in counter: counter.remove(prev_class)

                self.current_class = counter[idx % len(counter)]
                self.classes_visited = self.classes_visited[1:]+[self.current_class]
                self.n_samples_drawn = 0

            class_sample_idx = idx%len(self.image_dict[self.current_class])
            self.n_samples_drawn += 1

            out_img = self.transform(self.ensure_3dim(Image.open(self.image_dict[self.current_class][class_sample_idx])))
            return self.current_class, out_img
        else:
            return self.image_list[idx][-1], self.transform(self.ensure_3dim(Image.open(self.image_list[idx][0])))

    def __len__(self):
        return self.n_files
